(function() {
    fetch("crystalcastles.json")
        .then((res) => {
            return res.json();
        })
        .then((json) => {
            document.body.appendChild(albumList(json))
        });
})();

function create (elem) {
    return document.createElement(elem)
}

function trackList(album) {
    let wrapper = create("ul");
    album.tracks.forEach((track) => {
        let elem = create("li");
        elem.innerText = track;
        wrapper.appendChild(elem);
    });
    return wrapper;
}

function hideButton(context) {
    let button = create("button");
    button.innerText = "+";
    button.addEventListener("click", () => {
        context.classList.toggle("opened");
        if (context.classList.contains("opened")) {
            button.innerText = "-";
        } else {
            button.innerText = "+";
        }
    });
    return button;
}

function albumList(albums) {
    let wrapper = create("ol");
    albums.forEach((album) => {
        let elem = create("li");
        elem.innerText = album.name;
        elem.appendChild(hideButton(elem));
        let list = trackList(album);
        elem.appendChild(list);
        wrapper.appendChild(elem)
    });
    return wrapper;
}