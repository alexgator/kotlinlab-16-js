import java.io.File

class IO {
    companion object {
        fun readFile(path: String): String {
            return File(path).readText()
        }
    }
}