import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "Phone Book", value = "/")
class MyServlet : HttpServlet() {

    companion object {
        val RESOURCE_PATH = "/home/user/IdeaProjects/lab-16/src/main/resources/"
    }

    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {

        println(req.requestURI)

        when (req.requestURI) {
            "/"                     -> res.writer.print(DOM.createPage())
            "/crystalcastles.json"  -> res.writer.print(IO.readFile(RESOURCE_PATH + "crystalcastles.json"))
            "/style.css"            -> res.writer.print(IO.readFile(RESOURCE_PATH + "style.css"))
            "/scripts.js"           -> res.writer.print(IO.readFile(RESOURCE_PATH + "scripts.js"))
        }
    }
}