class DOM {
    companion object {
        private fun h1(node: String) = "<h1>$node</h1>"

        private fun body(node: String) = "<body>$node</body>"

        private fun css(): String = "<link rel=\"stylesheet\" href=\"style.css\">"

        private fun js(): String = "<script src=\"scripts.js\"></script>"

        private fun head(title: String, node: String) = "<head><title>$title</title>$node</head>"

        private fun html(node: String) = "<!DOCTYPE html>\n<html>$node</html>"

        fun createPage(): String {
            return html(head(
                    "Crystal Castles", css()
                ) + body(
                    h1("Crystal Castles") + js()
                )
            )
        }
    }
}